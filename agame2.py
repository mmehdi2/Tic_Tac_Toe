import sys
import copy
import random
import signal
import AI
import timeit

# import team23


class GAME:

    def __init__(self, pla1, pla2):
        self.board = []
        for i in range(9):
            row = ['-'] * 9
            self.board.append(row)
        self.lastmoves = [-1, -1]
        self.winner = []
        self.state = ['-'] * 9
        self.p1 = pla1
        self.p2 = pla2
        self.final_block_allow = []
        self.next_block = None

    def print_board(self):
        print('Current Board:', file=sys.stderr, flush=True)
        for j in range(9):
            if j > 0 and j % 3 == 0:
                print('==========================', end='\n', file=sys.stderr, flush=True)
            for i in range(9):
                if i > 0 and i % 3 == 0:
                    print('- ', end='', file=sys.stderr, flush=True)
                if self.board[j][i] == '-':
                    print('| ', end='', file=sys.stderr, flush=True)
                else:
                    print(self.board[j][i], end=' ', file=sys.stderr, flush=True)
            print('', end='\n', file=sys.stderr, flush=True)
        print('Boards Condition:', end='\n', file=sys.stderr, flush=True)
        for i in range(0, 9, 3):
            print(self.state[i] + ' ' + self.state[i + 1] + ' ' + self.state[i + 2], end='\n', file=sys.stderr,
                  flush=True)

    def blocks_allowed(self):
        if (0 <= self.lastmoves[1] <= 2) and self.lastmoves[0] % 3 == 0:
            blocks = [self.lastmoves[1]]
        elif (0 <= self.lastmoves[1] <= 2) and self.lastmoves[0] % 3 == 1:
            blocks = [self.lastmoves[1] + 3]
        elif (0 <= self.lastmoves[1] <= 2) and self.lastmoves[0] % 3 == 2:
            blocks = [self.lastmoves[1] + 6]
        elif (3 <= self.lastmoves[1] <= 5) and self.lastmoves[0] % 3 == 0:
            blocks = [self.lastmoves[1] - 3]
        elif (3 <= self.lastmoves[1] <= 5) and self.lastmoves[0] % 3 == 1:
            blocks = [self.lastmoves[1]]
        elif (3 <= self.lastmoves[1] <= 5) and self.lastmoves[0] % 3 == 2:
            blocks = [self.lastmoves[1] + 3]
        elif (6 <= self.lastmoves[1] <= 8) and self.lastmoves[0] % 3 == 0:
            blocks = [self.lastmoves[1] - 6]
        elif (6 <= self.lastmoves[1] <= 8) and self.lastmoves[0] % 3 == 1:
            blocks = [self.lastmoves[1] - 3]
        elif (6 <= self.lastmoves[1] <= 8) and self.lastmoves[0] % 3 == 2:
            blocks = [self.lastmoves[1]]

        print(blocks[0]+1, file=sys.stderr, flush=True)
        self.final_block_allow = []
        for block in blocks:
            if self.state[block] == '-':
                self.final_block_allow.append(block)


    def verification_board(self, temp_b):
        return self.state == temp_b

    def verification_cells(self, temp_c):
        return self.board == temp_c

    def free_positions(self):
        cells = []  # it will be list of tuples
        self.blocks_allowed()
        # Iterate over possible blocks and get empty cells
        for blocks in self.final_block_allow:
            id1 = blocks / 3
            id2 = blocks % 3
            for i in range(int(id1) * 3, int(id1) * 3 + 3):
                for j in range(id2 * 3, id2 * 3 + 3):
                    if self.board[i][j] == '-':
                        cells.append((i, j))
        # If all the possible blocks are full, you can move anywhere
        if not cells:
            new_blocks_allowed = []
            all_blocks_allowed = [0, 1, 2, 3, 4, 5, 6, 7, 8]
            for i in all_blocks_allowed:
                if self.state[i] == '-':
                    new_blocks_allowed.append(i)

            for blocks in new_blocks_allowed:
                id1 = blocks // 3
                id2 = blocks % 3
                for i in range(id1 * 3, id1 * 3 + 3):
                    for j in range(id2 * 3, id2 * 3 + 3):
                        if self.board[i][j] == '-':
                            cells.append((i, j))
        return cells

    def check_legal(self, move):
        if type(move) is not tuple:
            return False
        if len(move) is not 2:
            return False
        if type(move[0]) is not int or type(move[1]) is not int:
            return False
        if move[0] < 0 or move[0] > 8 or move[1] < 0 or move[1] > 8:
            return False
        if self.lastmoves[0] == -1 and self.lastmoves[1] == -1:
            return True
        cell = self.free_positions()
        if move in cell:
            return True
        else:
            return False

    def makemove(self, let, pos):
        self.board[pos[0]][pos[1]] = let
        block_no = (pos[0] // 3) * 3 + pos[1] // 3
        id1 = block_no // 3
        id2 = block_no % 3
        main_flag = 0
        flag = 0
        for i in range(id1 * 3, id1 * 3 + 3):
            for j in range(id2 * 3, id2 * 3 + 3):
                if self.board[i][j] == '-':
                    flag = 1
        if self.state[block_no] == '-':
            if self.board[id1 * 3][id2 * 3] == self.board[id1 * 3 + 1][id2 * 3 + 1] and self.board[id1 * 3 + 1][
                id2 * 3 + 1] == self.board[id1 * 3 + 2][id2 * 3 + 2] and self.board[id1 * 3 + 1][id2 * 3 + 1] != '-' \
                    and self.board[id1 * 3 + 1][id2 * 3 + 1] != 'D':
                main_flag = 1
            if self.board[id1 * 3 + 2][id2 * 3] == self.board[id1 * 3 + 1][id2 * 3 + 1] and self.board[id1 * 3 + 1][
                id2 * 3 + 1] == self.board[id1 * 3][id2 * 3 + 2] and self.board[id1 * 3 + 1][id2 * 3 + 1] != '-' and \
                    self.board[id1 * 3 + 1][id2 * 3 + 1] != 'D':
                main_flag = 1
            if main_flag != 1:
                for i in range(id2 * 3, id2 * 3 + 3):
                    if self.board[id1 * 3][i] == self.board[id1 * 3 + 1][i] and self.board[id1 * 3 + 1][i] == \
                            self.board[id1 * 3 + 2][i] and self.board[id1 * 3][i] != '-' \
                            and self.board[id1 * 3][i] != 'D':
                        main_flag = 1
                        break
            if main_flag != 1:
                for i in range(id1 * 3, id1 * 3 + 3):
                    if self.board[i][id2 * 3] == self.board[i][id2 * 3 + 1] and self.board[i][id2 * 3 + 1] == \
                            self.board[i][id2 * 3 + 2] and self.board[i][id2 * 3] != '-' \
                            and self.board[i][id2 * 3] != 'D':
                        main_flag = 1
                        break
        if flag == 0:
            self.state[block_no] = 'D'
        if main_flag == 1:
            self.state[block_no] = let
        return main_flag

    # def go_back(self):
    # self.board[self.lastmoves.pop()] = '-'

    def gameover(self):
        if '-' not in self.state:
            print('Game is a Draw', file=sys.stderr, flush=True)
            return True
        elif 'x' in self.state:
            print('Winner Is: X', file=sys.stderr, flush=True)
            return True
        elif 'o' in self.state:
            print('Winner Is: O', file=sys.stderr, flush=True)
            return True
        else:
            return False

    def location(self):
        if 0 <= self.lastmoves[1] <= 2 and (self.lastmoves[0] % 3 == 0):
            return self.lastmoves[1]
        elif 3 <= self.lastmoves[1] <= 5 and (self.lastmoves[0] % 3 == 0):
            return self.lastmoves[1] - 3
        elif 6 <= self.lastmoves[1] <= 8 and (self.lastmoves[0] % 3 == 0):
            return self.lastmoves[1] - 6
        if 0 <= self.lastmoves[1] <= 2 and (self.lastmoves[0] % 3 == 1):
            return self.lastmoves[1] + 3
        elif 3 <= self.lastmoves[1] <= 5 and (self.lastmoves[0] % 3 == 1):
            return self.lastmoves[1]
        elif 6 <= self.lastmoves[1] <= 8 and (self.lastmoves[0] % 3 == 1):
            return self.lastmoves[1] - 3
        if 0 <= self.lastmoves[1] <= 2 and (self.lastmoves[0] % 3 == 2):
            return self.lastmoves[1] + 6
        elif 3 <= self.lastmoves[1] <= 5 and (self.lastmoves[0] % 3 == 2):
            return self.lastmoves[1] + 3
        elif 6 <= self.lastmoves[1] <= 8 and (self.lastmoves[0] % 3 == 2):
            return self.lastmoves[1]

    def play(self):
        i = 0
        comptime = timeit.timeit()
        move = [-1, -1]
        while True:
            self.print_board()
            if i % 2 == 0:
                if self.p1.type == 'H':
                    while not self.check_legal(move):
                        print('Invalid Move', file=sys.stderr, flush=True)
                        move = self.p1.move()
                    self.makemove(self.p1.letter, move)
                    self.lastmoves = move
                elif self.p1.type == 'C':
                    start = timeit.timeit()
                    move = self.p1.move(self.board, self.state, self.lastmoves, 'x')
                    end = timeit.timeit()
                    comptime = comptime + abs(end-start)
                    self.makemove('x', move)
                    self.lastmoves = move
                    block_no = (move[0] // 3) * 3 + move[1] // 3
                    location = self.location()
                    print(str(block_no+1)+str(location))
            else:
                if self.p2.type == 'H':
                    while not self.check_legal(move):
                        print('Invalid Move', file=sys.stderr, flush=True)
                        move = self.p2.move()
                    self.makemove(self.p2.letter, move)
                    self.lastmoves = move
                elif self.p2.type == 'C':
                    start = timeit.timeit()
                    move = self.p2.move(self.board, self.state, self.lastmoves, 'o')
                    end = timeit.timeit()
                    comptime = comptime + abs(end - start)
                    self.makemove('o', move)
                    self.lastmoves = move
                    block_no = (move[0] // 3) * 3 + move[1] // 3
                    location = self.location()
                    print(str(block_no + 1) + str(location + 1))
            if self.gameover():
                self.print_board()
                print(comptime, file=sys.stderr, flush=True)
                return True
            i = i + 1


class HUMAN:
    def __init__(self, let):
        self.letter = let
        self.type = 'H'

    # noinspection PyBroadException
    @staticmethod
    def move():
        print('Input Position with (1-9) for board and then (1-9) for location in board: ', end='',
              file=sys.stderr, flush=True)
        inp = input()
        try:
            board = ((int(inp[0]) - 1) // 3) * 3 + ((int(inp[1]) - 1) // 3)
            move = ((int(inp[0]) - 1) % 3) * 3 + ((int(inp[1]) - 1) % 3)
        except:
            board = -1
            move = -1
        return board, move


if __name__ == '__main__':
    while True:
        while True:
            print('PICK X or O: ', end='', file=sys.stderr, flush=True)
            letter = input().lower()
            if letter == 'x':
                player1 = HUMAN('x')
                player2 = AI.COMP()
                break
            elif letter == 'o':
                player2 = HUMAN('o')
                player1 = AI.COMP()
                break
            else:
                print('Invalid Letter. Choose Again.', file=sys.stderr, flush=True)
        game = GAME(player1, player2)
        game.play()
        print('Do you want to play another game (Y/N) : ', end='', file=sys.stderr, flush=True)
        if input().lower() != 'y':
            break
