import sys
import timeit


class GAME:
    def __init__(self):
        self.board = []
        for i in range(0, 10):
            self.board.append('-')
        self.lastmoves = []
        self.winner = None
        self.p1 = []
        self.p2 = []
        self.board[0] = 'k'

    def print_board(self):
        print('Current Board:', file=sys.stderr, flush=True)
        for j in range(1, 10, 3):
            for i in range(3):
                if self.board[j + i] == '-':
                    print('| ', end='', file=sys.stderr, flush=True)
                else:
                    print(self.board[j + i], end=' ', file=sys.stderr, flush=True)
            print('', end='\n', file=sys.stderr, flush=True)

    def free_positions(self):
        moves = []
        for i, v in enumerate(self.board):
            if v == '-':
                moves.append(i)
        return moves

    def makemove(self, let, pos):
        self.board[pos] = let
        self.lastmoves.append(pos)

    def go_back(self):
        self.board[self.lastmoves.pop()] = '-'
        self.winner = None

    def gameover(self):
        win_pos = [(1, 2, 3), (4, 5, 6), (7, 8, 9), (1, 5, 9), (2, 5, 8), (3, 5, 7), (1, 4, 7), (3, 6, 9)]
        for i, j, k in win_pos:
            if self.board[i] == self.board[j] == self.board[k] and self.board[i] != '-':
                self.winner = self.board[i]
                return True
        if '-' not in self.board:
            self.winner = '-'
            return True
        return False

    def play(self, play1, play2):
        self.p1 = play1
        self.p2 = play2

        for i in range(0, 9):
            self.print_board()
            if i % 2 == 0:
                if self.p1.type == 'H':
                    print('\t[Your Move from (1-9)]', file=sys.stderr, flush=True)
                else:
                    print('\t[My Move]', file=sys.stderr, flush=True)
                self.p1.move(self)
            else:
                if self.p2.type == 'H':
                    print('\t[Your Move from (1-9)]', file=sys.stderr, flush=True)
                else:
                    print('\t[My Move]', file=sys.stderr, flush=True)
                self.p2.move(self)

            if self.gameover():
                self.print_board()
                if self.winner == '-':
                    print('DRAW', file=sys.stderr, flush=True)
                else:
                    print('\n Winner is : %s ' % self.winner, file=sys.stderr, flush=True)
                return


class HUMAN:
    def __init__(self, let):
        self.letter = let
        self.type = 'H'

    # noinspection PyBroadException
    def move(self, gameins):
        while True:
            print('Input Position: ', end='', file=sys.stderr, flush=True)
            inp = input()
            try:
                m = int(inp)
            except:
                m = -1
            if m not in gameins.free_positions():
                print('Invalid Move.', file=sys.stderr, flush=True)
            else:
                break
        gameins.makemove(self.letter, m)


class COMP:
    def __init__(self, let):
        self.letter = let
        self.type = 'C'
        if self.letter == 'X':
            self.opponent = 'O'
        else:
            self.opponent = 'X'
        self.gametime = 0

    def move(self, gameins):
        start = timeit.timeit()
        move_pos, score = self.max_move(gameins)
        end = timeit.timeit()
        self.gametime = self.gametime + abs(end-start)
        print('Total Comp time= '+ str(self.gametime), file=sys.stderr, flush=True)

        print(move_pos)
        gameins.makemove(self.letter, move_pos)

    def max_move(self, gameins):
        bestscore = None
        bestmove = None
        for mo in gameins.free_positions():
            gameins.makemove(self.letter, mo)
            if gameins.gameover():
                score = self.get_score(gameins)
            else:
                move_pos, score = self.min_move(gameins)
            gameins.go_back()
            if bestscore is None or score > bestscore:
                bestscore = score
                bestmove = mo
        return bestmove, bestscore

    def min_move(self, gameins):
        bestscore = None
        bestmove = None
        for mo in gameins.free_positions():
            gameins.makemove(self.opponent, mo)
            if gameins.gameover():
                score = self.get_score(gameins)
            else:
                move_pos, score = self.max_move(gameins)
            gameins.go_back()
            if bestscore is None or score < bestscore:
                bestscore = score
                bestmove = mo
        return bestmove, bestscore

    def get_score(self, gameins):
        if gameins.gameover():
            if gameins.winner == self.letter:
                return 1
            elif gameins.winner == self.opponent:
                return -1
        return 0


if __name__ == '__main__':
    while True:
        print('PICK X or O: ', end='', file=sys.stderr, flush=True)
        letter = input().upper()
        while True:
            if letter == 'X':
                player1 = HUMAN('X')
                player2 = COMP('O')
                break
            elif letter == 'O':
                player2 = HUMAN('O')
                player1 = COMP('X')
                break
            else:
                print('Invalid Letter. Choose Again.', file=sys.stderr, flush=True)
        game = GAME()
        game.play(player1, player2)
        print('Do you want to play another game (Y/N) : ', end='', file=sys.stderr, flush=True)
        if input().lower() != 'y':
            break
