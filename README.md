#BASIC TIC TAC TOE
The basic version of tic tac toe uses python 3.7 as the language. It prints the board to the stderr output
and the move taken on the stdout. The design that I built uses 3 classes each representing the different
aspects of the program that is needed for the program operation. I used the basic minimax algorithm for
this part as the added need for the other variations was not needed for a simple game like this. The
classes and their functions and operations are listed below:
1. GAME: This class handles the rules and gameplay of the Tic Tac Toe board and thus it is the
main class on which the operations and legal moves and empty slots are defined. This allows for
a simpler main program code and easier code management as it is the class object that does
not allow any nonlegal moves simplifying the overall design.
2. HUMAN: This class is made for handling the Human player and its letter/marker along with the
input and output required e.g. move value, error if move incorrect and finagling making the
move on the game board of all other conditions are met.
3. COMP: This class handles the basic mini-max algorithm. It has 4 main functions:
a. The move function handles the final call to be moved to the gameboard and calls the
max function to start the algorithm.
b. The max move function takes the current game class as an input and uses it to find all
legal moves and try them one by one until the terminal condition is met or if it is the
min move function is called for the other players turn. This is repeated until a terminal
location is found and the score returned after removing the turns made.
c. Min move job is to move to reduce the score and thus works like the max function but
in the opposite direction to minimize the value of the algorithm.
d. Both min_move and max_move functions run until the terminal condition are met.
To Run the basic Tic Tac Toe:
1. Navigate the terminal to the location of the file named Bgame.
2. Then on the terminal run the command:
python3 Bgame.py
3. This should start the interactive game menu asking to choose between ‘X’ or ‘O’
4. After the correct letter has been chosen the empty game board will print and the ‘X’ player will
make the first turn then the ‘O’ player.
5. This will continue until the game has ended or one side has won the game.
Improvements that could be made:
2
1. The alpha-beta pruning method could be used to improve the performance of the game, but it is
good and the total game time the computer takes for the entire play set is less a fraction of the
second so the need for advanced algorithms in not apparent.
2. The game board could use some tweaks to make it look better

#ADVANCED TIC TAC TOE
This advanced version of Tic Tac Toe is interesting as the number of possible moves in the game have
dramatically increased and the state space of the system has increased exponentially. Due to the
increased state space, the simple strategies and search solutions like basic mini-max and A* search
cannot work as the next move is dependent on the opponents last move. The increase also makes the
human version of the game more complex, as now it can increase the variability of the game outcome
and thus a game between two players of equal experience can often resolve into a winner or loser
situation whereas in Basic Tic Tac Toe the result between two perfect players will result in draws. Also, if
viewed from an algorithmic prospective the difference between advanced tic tac toe and Ultimate tic tac
toe is minimal, thus if the AI is developed to win Ultimate tic tac toe then the AI can mostly win in
Advanced tic tac toe. For the implementation of this game I had to change most of the AI and move it to
a separate file so that the code remains visible, some of the core functionality of the game board had to
be modified to work properly thus the code is changed from the Basic tic tac toe game. The classes and
their important functions are listed below with the reference to the Basic game made where ever it
coincides:
1. GAME: This class handles the rules and gameplay of the Tic Tac Toe board and thus it is the
main class on which the operations, legal moves and empty slots are defined. This allows for a
simpler main program code and easier code management as it is the class object that does not
allow any nonlegal moves simplifying the overall design. This class was changed to
accommodate the larger board by increasing the size of each row to 9 and each column to 9
thus an array of 9x9 elements is created which is logically divided into a game board of 3x3 tic
tac toe boards each of 3x3 elements. Additional functions were added to make sure the
management of the board is accurate logically and to decode the (board, location) value to
rows and columns and vice-versa. The game class also handles the overall code play state of the
game and thus makes sure the legal values are only values accepted.
2. HUMAN: This class is made for handling the Human player and its letter/marker along with the
input and output required e.g. move value, error if move incorrect and finally making the move
on the game board of all other conditions are met. This is a basic class that is only used to
handle the human input and make sure the input is in proper format and decoded to be used by
the rest of the game program.
3. COMP: This class handles the heuristic driven mini-max algorithm with alpha beta pruning and
uses Iterative Deepening search for the searching the game. Due to the increased complexity of
the AI, the AI is housed in a separate file to make the code easier to use and understand. The
class now has multiple functions and instead of sending the GAME class as done previously
which increased the logical complexity the current state of the game board is sent along with
the state of each board in the game. The AI is built for Ultimate tic tac toe, but it works for
Advanced tic tac toe as mentioned above. To make the program work really well I am using
tables and assigning values to special locations on the board so that the heuristics are easier to
find and do not consume a lot of compute resources during runtime and terminating before the
5
final depth due to time so that it plays in real time. There are many functions in the game, but
the main ones are listed below:
a. The move function which handles the AI game and other function calling and returns
the move that the AI is going to play. It also does iterative deepening search based on
timeout values and each time a timeout is reached the depth is decreased to minimize
the amount of time spent making a turn. This function calls the alphabeta function
which is used for the search of the game state.
b. The alphabeta (Russell) function is the main search function that takes in the current
state of the game and searches for all the optimal moves depending on the heuristics,
depth of the search and timeout function. This function is called recursively until the
terminal state is reached, the time runs out or the max depth for the search is reached.
And in any of these conditions the heuristics are called to give and approximate value of
the current state and the move which is sent back to so the max value for the AI is
always taken.
c. The genChild function is used to generate the children of the current node and returns
with the current node and the child of the current node after the current move is taken.
d. The heuristic_global (Patel)function is used to give the current global board the value of
according to the condition that the player (minimizing or maximizing) is at. This allows
for the entire board state each child to be evaluated as a value and thus allows for an
understanding of the current condition globally.
e. The heuristic_local function is used to give a value to the current local board state that
we are in thus reducing the need if near terminal state to see how good each move can
be.
f. The heuristic function which takes values stored in an list to evaluate the current board
value and thus its utility which is then returned
g. The other functions are basic and handle the game rules so the search is optimized and
create the lists used for heuristics.
To Run the Advanced Tic Tac Toe:
1. Navigate the terminal to the location of the file named agame2.py and in the same folder have
the file named AI.
2. Then on the terminal run the command:
python3 agame2.py
3. This should start the interactive game menu asking to choose between ‘X’ or ‘O’
4. After the correct letter has been chosen the empty game board will print and the ‘X’ player will
make the first turn then the ‘O’ player.
5. It will take 2 numbers e.g 15 as input and split the first and second digit for decoding it into
board and location values.
6. Will print the AI values to the stdout so 2 AI can play a game.
7. This will continue until the game has ended or one side has won the game.
Improvements that could be made:
1. The algorithm can be changed to improve the game performance by using Monte Carlo Tree
Search which will improve the heuristics and make the AI implementable in other scenarios. 
6
2. The game could be implement using a GUI making the playing of it more interactive and fun and
easier to understand for novices and people who don’t know how to play
3. The game board could use some tweaks to make it look better.
4. AI implementation could be limited by having better values for the heuristics.

#ULTIMATE TIC TAC TOE
This advanced version of Tic Tac Toe is interesting as the number of possible moves in the game have
dramatically increased and the state space of the system has increased exponentially. Due to the
increased state space, the simple strategies and search solutions like basic mini-max cannot work as the
next move is dependent on the opponents last move. The increase also makes the human version of the
game more complex, as now it can increase the variability of the game outcome and thus a game
between two players of equal experience can often resolve into a winner or loser situation whereas in
Basic Tic Tac Toe the result between two perfect players will result in draws. Also, if viewed from an
algorithmic prospective the difference between advanced tic tac toe and Ultimate tic tac toe is minimal,
thus if the AI is developed to win Ultimate tic tac toe then the AI can mostly win in Advanced tic tac toe.
The only major change required by my program is the terminal or game over state test as now to win 3
boards in a row must be won. The classes and their important functions are listed below with the
reference to the Basic game made where ever it coincides:
4. GAME: This class handles the rules and gameplay of the Tic Tac Toe board and thus it is the
main class on which the operations, legal moves and empty slots are defined. This allows for a
simpler main program code and easier code management as it is the class object that does not
allow any nonlegal moves simplifying the overall design. This class was changed to
accommodate the larger board by increasing the size of each row to 9 and each column to 9
thus an array of 9x9 elements is created which is logically divided into a game board of 3x3 tic
tac toe boards each of 3x3 elements. Additional functions were added to make sure the
management of the board is accurate logically and to decode the (board, location) value to
rows and columns and vice-versa. The game class also handles the overall code play state of the
game and thus makes sure the legal values are only values accepted.
5. HUMAN: This class is made for handling the Human player and its letter/marker along with the
input and output required e.g. move value, error if move incorrect and finally making the move
on the game board of all other conditions are met. This is a basic class that is only used to
handle the human input and make sure the input is in proper format and decoded to be used by
the rest of the game program.
6. COMP: This class handles the heuristic driven mini-max algorithm with alpha beta pruning and
uses Iterative Deepening search for the searching the game. Due to the increased complexity of
the AI, the AI is housed in a separate file to make the code easier to use and understand. The
class now has multiple functions and instead of sending the GAME class as done previously
which increased the logical complexity the current state of the game board is sent along with
the state of each board in the game. The AI is built for Ultimate tic tac toe, but it works for
Advanced tic tac toe as mentioned above. There are many functions in the game, but the main
ones are listed below:
a. The move function which handles the AI game and other function calling and returns
the move that the AI is going to play. It also does iterative deepening search based on
timeout values and each time a timeout is reached the depth is decreased to minimize 
the amount of time spent making a turn. This function calls the alphabeta function
which is used for the search of the game state.
b. The alphabeta (Russell) function is the main search function that takes in the current
state of the game and searches for all the optimal moves depending on the heuristics,
depth of the search and timeout function. This function is called recursively until the
terminal state is reached, the time runs out or the max depth for the search is reached.
And in any of these conditions the heuristics are called to give and approximate value of
the current state and the move which is sent back to so the max value for the AI is
always taken.
c. The genChild function is used to generate the children of the current node and returns
with the current node and the child of the current node after the current move is taken.
d. The heuristic_global (Patel)function is used to give the current global board the value of
according to the condition that the player (minimizing or maximizing) is at. This allows
for the entire board state each child to be evaluated as a value and thus allows for an
understanding of the current condition globally.
e. The heuristic_local function is used to give a value to the current local board state that
we are in thus reducing the need if near terminal state to see how good each move can
be.
f. The heuristic function which takes values stored in an list to evaluate the current board
value and thus its utility which is then returned
g. The other functions are basic and handle the game rules so the search is optimized and
create the lists used for heuristics.
To Run the Advanced Tic Tac Toe:
8. Navigate the terminal to the location of the file named agame2.py and in the same folder have
the file named AI.
9. Then on the terminal run the command:
python3 Ugame.py
10. This should start the interactive game menu asking to choose between ‘X’ or ‘O’
11. After the correct letter has been chosen the empty game board will print and the ‘X’ player will
make the first turn then the ‘O’ player.
12. It will take 2 numbers e.g 15 as input and split the first and second digit for decoding it into
board and location values.
13. Will print the AI values to the stdout so 2 AI can play a game.
14. This will continue until the game has ended or one side has won the game.
Improvements that could be made:
5. The algorithm can be changed to improve the game performance by using Monte Carlo Tree
Search which will improve the heuristics and make the AI implementable in other scenarios.
6. The game could be implement using a GUI making the playing of it more interactive and fun and
easier to understand for novices and people who don’t know how to play
7. The game board could use some tweaks to make it look better.
8. AI implementation could be limited by having better values for the heuristics.